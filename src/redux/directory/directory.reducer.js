const INITIAL_STATE = {
  sections: [
    {
      title: 'hats',
      imageUrl: 'https://image.freepik.com/free-photo/beach-hat-wooden-background_74190-2999.jpg',
      id: 1,
      linkUrl: 'shop/hats'
    },
    {
      title: 'jackets',
      imageUrl: 'https://image.freepik.com/free-photo/men-casual-trendy-fashion-with-black-jacket-accessories_1357-163.jpg',
      id: 2,
      linkUrl: 'shop/jackets'
    },
    {
      title: 'sneakers',
      imageUrl: 'https://image.freepik.com/free-photo/woman-making-legs-up_23-2147817477.jpg',
      id: 3,
      linkUrl: 'shop/sneakers'
    },
    {
      title: 'womens',
      imageUrl: 'https://image.freepik.com/free-photo/traditional-indian-attire-bangles-crossed-hands_8353-10051.jpg',
      size: 'large',
      id: 4,
      linkUrl: 'shop/womens'
    },
    {
      title: 'mens',
      imageUrl: 'https://image.freepik.com/free-photo/indian-business-man-with-crossed-arm-dark-wall_231208-2667.jpg',
      size: 'large',
      id: 5,
      linkUrl: 'shop/mens'
    }
  ]
}
const directoryReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state;
  }
}
export default directoryReducer;