const SHOP_DATA = {
  hats: {
    id: 1,
    title: 'Hats',
    routeName: 'hats',
    items: [
      {
        id: 1,
        name: 'Straw Hat',
        imageUrl: 'https://images.pexels.com/photos/1078973/pexels-photo-1078973.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        price: 35
      },

      {
        id: 2,
        name: 'Wild Animal Hat',
        imageUrl: 'https://images.pexels.com/photos/4890269/pexels-photo-4890269.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        price: 58
      },
      {
        id: 3,
        name: 'Decent Me',
        imageUrl: 'https://images.pexels.com/photos/4057684/pexels-photo-4057684.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        price: 57
      },
      {
        id: 4,
        name: 'SnowWhite',
        imageUrl: 'https://images.pexels.com/photos/3783091/pexels-photo-3783091.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        price: 32
      },
      {
        id: 5,
        name: 'Smart Dope',
        imageUrl: 'https://images.pexels.com/photos/1170980/pexels-photo-1170980.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        price: 29
      },
      {
        id: 6,
        name: 'Palm Tree Cap',
        imageUrl: 'https://images.pexels.com/photos/1263986/pexels-photo-1263986.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        price: 34
      },
      {
        id: 8,
        name: 'Cool Summer',
        imageUrl: 'https://images.pexels.com/photos/6547287/pexels-photo-6547287.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        price: 54
      },
    ]
  },
  sneakers: {
    id: 2,
    title: 'Sneakers',
    routeName: 'sneakers',
    items: [
      {
        id: 10,
        name: 'Sports Fit',
        imageUrl: 'https://image.freepik.com/free-photo/flat-lay-sneakers-with-copy-space_23-2148523298.jpg',
        price: 220
      },
      {
        id: 11,
        name: 'Black Gum',
        imageUrl: 'https://image.freepik.com/free-photo/black-fashion-gumshoes_144627-9653.jpg',
        price: 280
      },
      {
        id: 12,
        name: 'College Fuzzy',
        imageUrl: 'https://image.freepik.com/free-photo/man-sneakers_144627-9793.jpg',
        price: 110
      },
      {
        id: 13,
        name: 'Black shoot',
        imageUrl: 'https://image.freepik.com/free-photo/sportsman-feet-playing-football-stadium_23-2148203689.jpg',
        price: 560
      },
      {
        id: 14,
        name: 'Indie Orange',
        imageUrl: 'https://image.freepik.com/free-photo/crop-woman-tying-laces-sneakers_23-2147755539.jpg',
        price: 960
      },
      {
        id: 15,
        name: 'Perfect Blue',
        imageUrl: 'https://image.freepik.com/free-photo/pair-trainers_144627-3811.jpg',
        price: 60
      },
    ]
  },
  jackets: {
    id: 3,
    title: 'Jackets',
    routeName: 'jackets',
    items: [
      {
        id: 18,
        name: 'Black Jean Shearling',
        imageUrl: 'https://image.freepik.com/free-photo/photo-beautiful-young-blond-girl-with-curly-hair-closeup-attractive-sensual-face-white-woman-with-long-hair-smokey-eye-makeup_186202-4563.jpg',
        price: 443
      },
      {
        id: 22,
        name: 'Casual Blue',
        imageUrl: 'https://image.freepik.com/free-photo/amazed-happy-guy-with-overjoyed-expression-keeps-hands-face-gazes-camera-with-unexpected-cheerful-look_273609-18930.jpg',
        price: 385
      },
      {
        id: 19,
        name: 'White Jacket',
        imageUrl: 'https://image.freepik.com/free-photo/side-view-young-woman-wearing-winter-clothes_23-2148841791.jpg',
        price: 94
      },
      {
        id: 20,
        name: 'Red Velvet Jacket',
        imageUrl: 'https://image.freepik.com/free-photo/beautiful-sexy-woman-with-bright-red-bob-hairstyle-fashion-model-sensual-gorgeous-girl-leather-jacket-stunning-face-pretty-lady_186202-8465.jpg',
        price: 34
      },
      {
        id: 21,
        name: 'Denim Jacket',
        imageUrl: 'https://image.freepik.com/free-photo/smiley-woman-with-denim-jacket_23-2148574856.jpg',
        price: 78
      }

    ]
  },
  womens: {
    id: 4,
    title: 'Womens',
    routeName: 'womens',
    items: [
      {
        id: 23,
        name: 'Golden dress',
        imageUrl: 'https://image.freepik.com/free-photo/young-woman-with-shopping-bags-beautiful-dress_1303-17550.jpg',
        price: 35
      },
      {
        id: 24,
        name: 'Red saree',
        imageUrl: 'https://image.freepik.com/free-photo/indian-beauty-face-big-eyes-with-perfect-wedding_121764-394.jpg',
        price: 210
      },

      {
        id: 26,
        name: 'Black  Dress',
        imageUrl: 'https://image.freepik.com/free-photo/gorgeous-elegant-sensual-blonde-woman-wearing-fashion-black-dress_149155-242.jpg',
        price: 82
      },
      {
        id: 27,
        name: 'Violet saree',
        imageUrl: 'https://image.freepik.com/free-photo/young-indian-woman-sitting-floor-looking-side_1368-96413.jpg',
        price: 415
      },
      {
        id: 29,
        name: 'Brown Sweater',
        imageUrl: 'https://image.freepik.com/free-photo/puzzled-young-pretty-brunette-woman-with-casual-hairstyle-biting-underlip-keeping-raised-hand-her-chin-while-looking-dreamily-upwards-posing-pink-wall_295783-10626.jpg',
        price: 29
      }
    ]
  },
  mens: {
    id: 5,
    title: 'Mens',
    routeName: 'mens',
    items: [

      {
        id: 31,
        name: 'Kurta',
        imageUrl: 'https://image.freepik.com/free-photo/handsome-man-celebrating-tet_274689-9446.jpg',
        price: 20
      },
      {
        id: 32,
        name: 'White T-Shirt',
        imageUrl: 'https://cdn.shopify.com/s/files/1/1370/7529/products/PracticeTee-Shirtlightgrey3_540x.jpg?v=1609168961',
        price: 125
      },
      {
        id: 33,
        name: 'Navy Blue T-shirt',
        imageUrl: 'https://image.freepik.com/free-psd/front-view-man-wearing-t-shirt_23-2148457385.jpg',
        price: 65
      },
      {
        id: 35,
        name: 'Blue shirt',
        imageUrl: 'https://image.freepik.com/free-photo/handsome-man-posing_144627-18969.jpg',
        price: 32
      }
    ]
  }
};

export default SHOP_DATA;